# XService

## 介绍

XService 是 Ubuntu 系统下服务管理工具，以组管理 systemd 服务，支持服务（组）的增删查改，启动与停止，使能与失能开机自启以及状态查看。XService 支持与远程主机的后台服务组进行交互。

## 一、安装步骤

### 1.1  安装与卸载

克隆仓库，赋可执行权限，拷贝至 /usr/local/bin

```bash
# 安装
git clone https://gitee.com/Lxk_buaa/xservice.git
cd xservice/
chmod +x xservice
sudo cp xservice /usr/local/bin/xservice

# 卸载
# sudo rm /usr/local/bin/xservice
```

确认

```python
xservice
# 等同于 xservice -h 或 xservice --help
```

安装成功，会提示版本与参数，例如

```bash
xservice v1.0.0 (2024-07-26)
Author: Xiaokang Lv
Email: lv_xiaokang@buaa.edu.cn

Usage: xservice [COMMAND] [OPTIONS]

Commands:
  --help, -h          Help
  --create, -c        Create xservice
  --create_after_services, -cs Create xservice after service
  --delete, -d        Delete xservice (stop and disable in advance)
  --list, -l          List the service file contents of xservice
  --edit, -e          Edit the service file of xservice
  --start, -s         Start xservice
  --restart, -r       Restart xservice
  --stop, -t          Stop xservice
  --enable, -E        Enable xservice to start automatically at boot
  --disable, -D       Disable xservice from starting automatically at boot
  --status, -S        Display the current status of xservice
  --attach, -a        Attach the current terminal to xservice

Use 'xservice --help [COMMAND]' for more information about a command.
```

更具体的帮助【TODO】

```bash
xservice -h --create
xservice -h --attach
...
```

### 1.2  更新命令

```python
cd ~/xservice
git pull
sudo cp -f xservice /usr/local/bin/xservice
```

## 二、基础功能

### 2.1 服务（组）的增删查改

#### 创建

在 /etc/systemd/system 目录下，根据指定命令，创建服务文件。在服务文件创建后，会自动执行 sudo systemctl daemon-reload，但不会启动和设置开机自启服务。

例：以下命令，将在组 1 下创建一个服务，命名为 my_top（同组且重名服务文件会覆盖），执行的命令是 top，检查cpu占用率。

```bash
sudo xservice -c 1 "my_top" "top"
```

需要 sudo 权限，否则无写入权限。执行成功的输出如下，其中包括生成的服务文件的路径，并提示已经进行 sudo systemctl daemon-reload。

```bash
Creating service file at /etc/systemd/system/xs1_my_top.service
Reloading systemd daemon
```

例：以下命令，将在组 2 下创建多个服务

```bash
sudo xservice -c 2 "roscore" "source /opt/ros/noetic/setup.bash && roscore"
sudo xservice -c 2 "sub_command" "source /opt/ros/noetic/setup.bash && rostopic echo /command" "rosout"
```

其中，第一个命令创建一个无依赖的服务，将会在服务启动时，立即运行。

```bash
source /opt/ros/noetic/setup.bash && roscore
```

第二个命令创建一个服务，依赖 rosout 节点。可以视为服务启动后，会在 roscore 启动完毕后才执行

```bash
source /opt/ros/noetic/setup.bash && rostopic echo /command
```

例：多 ROS 节点依赖的 roslaunch 服务设置如下。只有多个依赖节点都启动后才会开始执行命令。可以使用 && 或 ; 间隔多条命令，完成刷新环境变量，补充设置环境变量，roslaunch等多步操作。

```bash
sudo xservice -c 2 "AAA" \
"source BBB/devel/setup.bash && \
export CCC=DDD && \
roslaunch EEE FFF.launch GGG:=HHH" \
"rosout III JJJ/KKK"
```

注意事项：一些包含图形界面的ros节点的服务，如rviz、xgc、turtlesim，可能需要额外设置环境变量，否则服务会启动失败。

#### **删除**

删除由 xservice 创建的服务文件。此步骤会先结束服务与失能开机自启。在服务文件删除后，会自动执行 sudo systemctl daemon-reload。

例：

```bash
sudo xservice -d 1 "AAA" # 删除组1下名字为AAA的服务文件，名字应与创建时对应，不是服务文件完整名称。
sudo xservice -d 2       # 删除组2所有服务文件
sudo xservice -d         # 删除所有由xservice创建的服务文件
```

执行后，会提示具体删除的文件，需要输入y进行确认。

```bash
The following service files will be deleted:

/etc/systemd/system/xs1_my_top.service
/etc/systemd/system/xs2_roscore.service
/etc/systemd/system/xs2_sub_command.service

Do you want to continue and delete these files? (y/n) 
```

#### 查询

打印由 xservice 创建的服务文件内容以及服务当前状态等信息。

例：

```bash
xservice -l 1 "AAA"  # 列出组1下名字为AAA的服务文件内容信息
xservice -l 2        # 列出组2所有服务文件内容信息
xservice -l          # 列出所有由xservice创建的服务文件内容信息
```

执行效果如下，绿色为正在运行，蓝色为未运行但使能开机自启。

```bash
========================================
Service Path: /etc/systemd/system/xs1_my_top.service
Group: 1
Name: my_top
Enabled: No
Active: No
----------------------------------------
[Unit]
Description=XService my_top
After=network.target

[Service]
Type=forking
User=lxk
ExecStart=/bin/bash -c 'xservice --run 1 my_top "top"'

[Install]
WantedBy=multi-user.target
========================================
Service Path: /etc/systemd/system/xs2_roscore.service
Group: 2
Name: roscore
Enabled: No
Active: No
----------------------------------------
[Unit]
Description=XService roscore
After=network.target

[Service]
Type=forking
User=lxk
ExecStart=/bin/bash -c 'xservice --run 2 roscore "source /opt/ros/noetic/setup.bash && roscore"'

[Install]
WantedBy=multi-user.target
========================================
```

其中

- Service Path 是服务文件绝对路径
- Group 是服务文件在xservice的组
- Name 是服务文件在xservice的名称，可以指定删除 sudo xservice -d 1 "my_top"
- Enabled 是否开机自启
- Active 是否正在运行

例：-s 可以打印服务的历史记录，包括启停时间与进程输出内容。使用方法同 -l

```bash
xservice -S 1 "AAA"  # 列出组1下名字为AAA的服务状态
xservice -S 2        # 列出组2所有服务状态
xservice -S          # 列出所有由xservice创建的服务状态
```

#### 编辑

修改由 xservice 创建的服务文件内容。

例：编辑指定服务文件

```bash
# 使用默认编辑器 nano 打开文件
sudo xservice -e 2 "roscore" # 默认使用 nano 编辑器
# 编辑文件后：
# 1. 保存更改：按 `Ctrl + O`，然后按 `Enter` 确认文件名。
# 2. 退出编辑器：按 `Ctrl + X`。

# 使用 vim 编辑器打开文件
sudo xservice -e 2 "AAA" vim   # 指定 vim 编辑器
# 编辑文件后：
# 1. 保存更改：按 `Esc` 进入普通模式，然后输入 `:w` 并按 `Enter`。
# 2. 退出编辑器：按 `Esc` 进入普通模式，然后输入 `:q` 并按 `Enter`。
# 3. 如果需要保存并退出：按 `Esc` 进入普通模式，然后输入 `:wq` 并按 `Enter`。
# 4. 如果不保存并退出：按 `Esc` 进入普通模式，然后输入 `:q!` 并按 `Enter`。

# 使用 gedit 编辑器打开文件
sudo xservice -e 2 "my_top" gedit # 指定 gedit 编辑器
```

### 2.2 服务（组）的启停

按组启动与停止服务（推荐）

```bash
xservice -r 1 # 重启(restart)组1所有服务
xservice -t 1 # 或 xservice -t 停止(stop)所有由xservice创建的服务
```

其他

```bash
xservice -s 1 # 启动(start)组1所有服务，如果已经启动则不会重启

# 支持以下形式输入
xservice -r 1 "AAA"
xservice -t 1 "AAA"
xservice -s 1 "AAA" 
xservice -r 1
xservice -t 1
xservice -s 1
xservice -r
xservice -t
xservice -s
```

### 2.3 服务（组）的开机自启

按组使能与失能开机自启（推荐）

```bash
xservice -E 1 # 使能(enable)组1所有服务
xservice -D 1 # 或 xservice -t 失能(disable)所有由xservice创建的服务
```

其他

```bash
# 支持以下形式输入
xservice -E 1 "AAA"
xservice -D 1 "AAA"
xservice -E 1
xservice -D 1
xservice -E
xservice -D
```

目前直接使能算法组的开机自启存在个别节点启动失败的问题，需要按以下方式设置开机自启。创建一个组2来运行组1，并将组2设置为开机自启！

```bash
# 组1下创建若干算法
# sudo xservice -c 1 "roscore" "source /opt/ros/noetic/setup.bash && roscore"
# sudo xservice -c 1 "sub_command" "source /opt/ros/noetic/setup.bash && rostopic echo /command" "rosout"

# 不要将组1设为开机自启
sudo xservice -D 1

# 创建一个组2，注意将 password 改为机载电脑的密码
sudo xservice -c 2 "start_group1" "echo "password" | sudo -S xservice -s 1"

# 将组2设置为开机自启
sudo xservice -E 1
```




### 2.4 附加服务组会话

附加本机服务组会话到本地终端，可以按组将服务启动的后台进程附加到交互式终端。一个组服务对应一个终端中的一个窗口（window），包含多个窗格（pane），组内每个服务进程对应一个窗格，可以通过窗格进行交互。

例：将服务组1附加到当前交互式终端

```bash
xservice -a 1
```

完整流程如下：

在组1下，创建roscore、发布者与订阅者服务，其中后两个服务指定依赖rosout节点，即等待roscore启动完毕。

```bash
sudo xservice -c 1 "roscore" "source /opt/ros/noetic/setup.bash && roscore" && \
sudo xservice -c 1 "sub_command" "source /opt/ros/noetic/setup.bash && rostopic echo /command" "rosout" && \
sudo xservice -c 1 "pub_command" "source /opt/ros/noetic/setup.bash && rostopic pub -r 10 /command std_msgs/Int64 \"data: 2\"" "rosout"
```

启动组1

```bash
xservice -r 1
```

启动成功提示

```bash
Starting service: xs1_pub_command
Starting service: xs1_roscore
Starting service: xs1_sub_command
Restart command processing completed.
```

将附加组1到当前终端

```bash
xservice -a 1
```

直接关闭整个窗口不会结束后台程序，可以随时关闭与再次附加。可以通过窗格结束后台程序。窗格大小调整与焦点切换可以参考tmux使用说明。

![Untitled](pictures/attach.png)


附加到当前终端的还可以是远程主机的服务，可参考

例：在当前终端显示远程主机的服务组1的后台进程

```bash
# ssh登录远程主机后使用xservice --attach
ssh username@192.168.XXX.XXX;
xservice -a 1
```

注意事项：SSH 的断连与直接关闭终端不会结束任何后台进程，但是可以在窗格中使用 ctrl + C 会结束对应的后台进程。


## 三、使用场景

### 3.1 ROS 节点开机自启

例：一般驱动或算法部署【TODO】

例：XGC 天空端的部署【TODO】

### 3.2 算法切换

例：比赛科目间切换【TODO】

### 3.3 附加远程服务组的会话到本地终端

例：XGC 地面站起飞检查【TODO】


## 四、问题解决
服务长时间不启动，可以使用 systemd 的 status 查看服务日志，比如报错服务启动超时

```
Job for xs1_roscore.service failed because a timeout was exceeded.
See "systemctl status xs1_roscore.service" and "journalctl -xe" for details.
```
检查

```
xservice[3635]: error connecting to /tmp/tmux-xservice/default (Permission denied)
```

解决方法
```
sudo chmod 777 /tmp/tmux-xservice/default
```


## 参与贡献
如果您对该项目有任何疑问、建议或者问题，请随时联系维护者 - [Xiaokang Lv](mailto:lv_xiaokang@buaa.edu.cn)
